import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false


import Lodding from "@/components/Lodding.vue";
Vue.component('Lodding',Lodding)

import './rem.js'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
