// (function (win) {
//     var docEl = win.document.documentElement;
//     var time;
//     function refreshRem() {
//         var width = docEl.getBoundingClientRect().width;
//         if (width > 768) { // 最大宽度
//             width = 768;
//         }
//         var rem = width / 375 * 100;
//         docEl.style.fontSize = rem + 'px';
//         ///rem用font-size:50px来进行换算
//     }
//     win.addEventListener('resize', function () {
//         clearTimeout(time);
//         time = setTimeout(refreshRem, 1);
//     }, false);
//     win.addEventListener('pageshow', function (e) {
//         if (e.persisted) {
//             clearTimeout(time);
//             time = setTimeout(refreshRem, 1);
//         }
//     }, false);
//     refreshRem();
// })(window);

const setView = () => {
    let clientWidth = document.documentElement.clientWidth
    if (clientWidth >= 1024) {
        clientWidth = 1024
    }
    document.documentElement.style.fontSize = (100 * clientWidth) / 375 + 'px';
}
window.onresize = setView
setView()